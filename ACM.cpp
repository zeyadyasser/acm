//--------ACM TRICKS--------

//-Maths tricks-

//-Newton’s method(finds roots of any fn. easily):
double func(double x){
    return x*x; // x^2 example function
}
double diff_func(double x){
    return 2*x; // function derivative
}
vector<double> newton_method(vector<double> values, double abs_err){
    // abs_err specifies accepted error
    for(int i=0; i<values.size(); i++){
        while( abs(func(values[i])) > abs_err ){
            values[i] = values[i] - (func(values[i])/diff_func(values[i]));
        }
    }
    return values;
}
int main(){
    // Example
    vector<double> values = {-9999, 0};
    values = newton_method(values, 1e-8);
    cout << values[0] << " " << values[1] << endl;
    return 0;
}

//-Modular exponent( B^P MOD M ): #O(LOG(P))
unsigned int mod_pow(unsigned int b, unsigned int p , unsigned int m){
    if(p == 0)
            return 1%m;
    else if(p == 1)
            return b%m;
    else if(p%2 == 0){
        unsigned int x = mod_pow(b, p/2, m) % m;
        return (x*x)%m;
    }
    else{
            return ((b%m) * mod_pow(b, p-1, m)) % m;
    }
}

//-Fibonacci: #O(1)
const double PHI = (1+sqrt(5))/2;
unsigned long long fib(unsigned long long n){
    return floor(pow(PHI, n)/sqrt(5) + 0.5);
}


//-Sieve: #O(N.loglog(N))
bool is_prime[10000000];

void sieve(int n)
{
    is_prime[0] = is_prime[1] = false;

    // optimize by computing multiples of
    // 2 then incerementing by 2 in the
    // main loop
    for(int i=4; i<=n; i+=2)
        is_prime[i] = false;

    for(int i=3; i*i<=n; i+=2)
    {
        if(is_prime[i])
        {
            for(int j=i*2; j<=n; j+=i)
                is_prime[j] = false;
        }
    }
}

int main()
{
    memset(is_prime, true, sizeof(is_prime));
    sieve(10000000);
}

//-Matrix Exponentiation O(N^3.log(P))
// Can be user with matrices with small size (N)
// to compute the power P of the matrix
// NOTE: Use MOD for large values;

const int MOD = INT_MAX;

template <class T>
struct Matrix
{
    vector< vector<T> > mat;
    int n_rows, n_cols;
    Matrix(int rows, int cols, T init = -1)
    {
        if(init == -1)
            mat = vector< vector<T> >(rows, vector<T>(cols));
        else
            mat = vector< vector<T> >(rows, vector<T>(cols, init));
        n_rows = rows;
        n_cols = cols;
    }

    Matrix identity_matrix(int n)
    {
        Matrix I(n, n, 0);
        for(int i=0; i<n; i++)
            I.mat[i][i] = 1;
        return I;
    }

    Matrix operator*(const Matrix &other)
    {
        assert(n_cols == other.n_rows);

        int n = n_rows, m = other.n_cols;
        Matrix result(n, m);

        for(int i=0; i<n; i++)
        {
            for(int j=0; j<m; j++)
            {
                for(int k=0; k<n_cols; k++)
                    result.mat[i][j] = (result.mat[i][j] + mat[i][k] * 1ll * other.mat[k][j]) % MOD;
            }
        }

        return result;
    }

    Matrix exp(long long p)
    {
        assert(n_cols == n_rows);

        Matrix m = *this;
        Matrix result = identity_matrix(n_cols);

        while(p)
        {
            if(p & 1)
                result = result * m;
            m = m * m;
            p = (p>>1);
        }
        return result;
    }

};

//-C++ Tricks-

//-Int to String
#include <sstream>
string IntToString (int a){
    ostringstream temp;
    temp << a;
    return temp.str();
}

//-Algo Tricks-

//-Binary Search (Integer)
//TTTTTTFFFFFFF
int bin_search(int st, int ed){
    int mid;
    while(st < ed){
        mid = st + (ed - st + 1)/2;
        if(!valid(mid))
            ed = mid - 1;
        else{
            st = mid;
        }
    }
    return st;
}
//FFFFFFFTTTTTT
int bin_search(int st, int ed){
    int mid;
    while(st < ed){
        mid = st + (ed - st)/2;
        if(valid(mid))
            ed = mid;
        else{
            st = mid+1;
        }
    }
    return st;
}

//-Binary Search (double)
//FFFFFFFTTTTTT
double bin_search(double st, double ed, double eps){
    double sz = ed - st;
    double mid;
    for(; sz > eps; sz/=2){
        mid = st + sz/2;
        if(!valid(mid))
            st = mid;
    }
return st;
}

//-Ternary Search (double)
//Finds the minimum of func(x)
double ternary_search(double st, double ed, double eps){
    double sz = ed - st;
    double a, b;
    for(; sz > eps; sz=((sz*2)/3)){
        a = st + sz/3;
        b = st + (sz*2)/3;
        if(func(a) > func(b))
            st = a;
    }
    return st;
}

//-Max Subarray: #O(N)
struct subarray{
    int left;
    int right;
    int sum;
};

subarray find_maxsubarray(int A[], int low, int high){
    subarray max_subarray;
    max_subarray.left = 0;
    max_subarray.right = 0;
    max_subarray.sum = A[low];

    int temp_sum = 0, temp_left = 0;

    for(int i=low+1; i<=high; i++){
        temp_sum = max(A[i], A[i] + temp_sum);
        if(temp_sum == A[i])
            temp_left = i;
        if(temp_sum > max_subarray.sum){
            max_subarray.right = i;
            max_subarray.left = temp_left;
            max_subarray.sum = temp_sum;
        }
    }
    return max_subarray;
}
